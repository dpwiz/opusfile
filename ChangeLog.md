# Changelog for opusfile

## [0.1.0.1]

- Fixed missing `_li` argument in `op_channel_count` and `op_pcm_total` FFI.

## [0.1.0.0] Initial import

[0.1.0.0]: https://gitlab.com/dpwiz/opusfile/-/tree/v0.1.0.0
