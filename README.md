# opusfile

https://opus-codec.org/

> Opus is a totally open, royalty-free, highly versatile audio codec.
> Opus is unmatched for interactive speech and music transmission over the Internet,
> but is also intended for storage and streaming applications.

My intention were to use the Opus codec for game assets with OpenAL.
There's `examples/openal-playfile` exactly for this.

You'll need `libopusfile-dev` or something like that for your distro.

Add to package dependencies and import `Sound.OpusFile` qualified.
